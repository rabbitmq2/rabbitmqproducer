package cpm.techmojo.producer.controller;

import java.util.UUID;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cpm.techmojo.producer.constants.AppConstants;
import cpm.techmojo.producer.dto.Order;
import cpm.techmojo.producer.dto.OrderStatus;

@RestController
@RequestMapping("/order")
public class OrderController {

	@Autowired
	private RabbitTemplate templete;

	@PostMapping("/{restaurentName}")
	public String placeOrder(@RequestBody Order order, @PathVariable String restaurentName) {

		order.setOrderId(UUID.randomUUID().toString());
		StackTraceElement[] methodStack = Thread.currentThread().getStackTrace();
		OrderStatus status = new OrderStatus(order, "IN-PROCESS", "Order placed successfully in " + restaurentName,
				methodStack);
		// producer..
		templete.convertAndSend(AppConstants.EXCHANGE, AppConstants.BINDING_KEY, status);
		return "Success!!";
	}

	//added for testing git commands..
	public String fetchData() {
		return "success!";
	}
}
