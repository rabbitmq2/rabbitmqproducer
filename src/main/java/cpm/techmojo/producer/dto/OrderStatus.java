package cpm.techmojo.producer.dto;

import java.util.Arrays;

public class OrderStatus {

	private Order order;
	private String status;
	private String message;
	private StackTraceElement[] methodStack;

	public OrderStatus(Order order, String status, String message, StackTraceElement[] methodStack) {
		super();
		this.order = order;
		this.status = status;
		this.message = message;
		this.methodStack = methodStack;
	}

	public OrderStatus() {
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public StackTraceElement[] getMethodStack() {
		return methodStack;
	}

	public void setMethodStack(StackTraceElement[] methodStack) {
		this.methodStack = methodStack;
	}

	@Override
	public String toString() {
		return "OrderStatus [order=" + order + ", status=" + status + ", message=" + message + ", methodStack="
				+ Arrays.toString(methodStack) + "]";
	}

}
