package cpm.techmojo.producer.constants;

public interface AppConstants {

	String QUEUE = "order-producer-queue";
	String EXCHANGE = "order-producer-exchange";
	String BINDING_KEY = "order-producer-bindingKey";
}
